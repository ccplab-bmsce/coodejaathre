void input(int *a, int *b)
{
    printf("Enter two numbers\n");
    scanf("%d%d",a,b);
}

unsigned int abs(int a)
{
    return a<0?-a:a;
}

unsigned int add(int a, int b)
{
    return abs(a)+abs(b);
}

void output(int a, int b, unsigned int sum)
{
    printf("sum of abs(%d) and abs(%d) is %u\n",a,b,sum);
}

int main()
{
    int a,b;
    unsigned int sum;
    input(&a,&b);
    sum=add(a,b);
    output(a,b,sum);
    return 0;
}